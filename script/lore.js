/*global $, jQuery */

// The Lore Studio namespace currently contains behavior for the single page Lore Studio application
// When new behavior is introduced subnamespaces are to be refactored into their own files
// © Charles A. McCown, 2016

// define the lore namespaces
var lore = window.lore;
if (typeof (lore) === 'undefined') {
    lore = {};
    window.lore = lore;
}
if (typeof (lore.controls) === 'undefined') { lore.controls = {}; }
if (typeof (lore.ui) === 'undefined') { lore.ui = {}; }


lore.controls.initalize = function () {
    // use strict mode as a sanity check for stupid things
    'use strict';
    
    lore.displayBtns = $('.display-btn');
    lore.displayBtns.clickHandler = function () {
        $('.display-container').slideUp();
        $('#' + $(this).data('display-target')).slideDown();
    };
    lore.displayBtns.click(lore.displayBtns.clickHandler);
    
    // the Lore Studio logo
    lore.logo = $(".logo");
    lore.logo.svg = $(".logo-svg");
    lore.logo.hintPointer = $('#svg-pointer');
    lore.logo.hoverHandler = function () {
        lore.logo.svg.trigger('hover');
        lore.logo.removeClass('attention');
        lore.logo.hintPointer.removeClass('hint');
    };
    lore.logo.clickHandler = function () {
        if (lore.revealables.revealed) {
            lore.sections.fadeOut();
            lore.vanishables.vanish();
            lore.logo.moveTop('50%', 250);
            lore.revealables.revealed = false;
        } else {
            setTimeout(function () { lore.sections.fadeIn(); }, 250);
            lore.revealables.reveal();
            lore.logo.moveTop('25%');
            lore.revealables.revealed = true;
        }
    };
    lore.logo.moveTop = function (value, pauseInMs) {
        if (typeof (pauseInMs) === 'undefined') { pauseInMs = 0; }
        
        setTimeout(function () {
            lore.logo.animate({ top : value, 'animation-timing-function' : 'ease-out'});
            lore.logo.svg.animate({ top : value, 'animation-timing-function' : 'ease-out'});
        }, pauseInMs);
    };
    lore.logo.hover(lore.logo.hoverHandler);
    lore.logo.click(lore.logo.clickHandler);
    
    
    // any pulsing containers
    lore.pulse = $(".pulse-container");
    
    // anything which can be revealed should be revealable
    // Warning: currently all revealable/vanishable controls are also pulse containers
    // Remediate this when time permits by adding a reveal-css data element to store any
    // css which must be removed to protect the reveal/vanish behavior.
    lore.revealables = $(".revealable");
    lore.revealables.reveal = function () {
        $.each(lore.revealables, function (index, revealable) {
            setTimeout(function () {
                $(revealable).addClass('pulse-container').animate(lore.ui.alpha($(revealable).data('opacity')));
            }, 500 * index);
        });
    };
    lore.revealables.revealed = false;
    
    // sections set aside to store content
    lore.sections = $('.sections');
    lore.sections.fadeOut(0);
    
    // todo: develop tooltip control here.
    lore.tooltips = $('[data-toggle="tooltip"]');
    
    // anything which can vanish should be revealable
    // Warning: currently all revealable/vanishable controls are also pulse containers
    // Remediate this when time permits by adding a reveal-css data element to store any
    // css which must be removed to protect the reveal/vanish behavior.
    lore.vanishables = $(".vanishable");
    lore.vanishables.removeClass('pulse-container').css(lore.ui.alpha(0));
    lore.vanishables.vanish = function () {
        $.each(lore.vanishables, function (index, vanishable) {
            setTimeout(function () {
                $(vanishable).removeClass('pulse-container').animate(lore.ui.alpha(0));
            }, 500 * index);
        });
    };
};

// lore ui contains useful methods for interacting with the ui
lore.ui.initalize = function () {
    // use strict mode as a sanity check for stupid things
    'use strict';
    
    // use alpha to generate css for setting the alpha for an element
    lore.ui.alpha = function (value) {
        return {
            'opacity': value,
            '-ms-filter': "progid:DXImageTransform.Microsoft.Alpha(Opacity=" + value + ")",
            'filter': 'alpha(opacity=' + value + ')'
        };
    };
    
};

// initialize the lore namespace on page load
lore.initialize = function () {
    // use strict mode as a sanity check for stupid things
    'use strict';
    
    // initialize ui
    lore.ui.initalize();
    
    // initialize controls
    lore.controls.initalize();
};

$(document).ready(function () {
    // use strict mode as a sanity check for stupid things
    'use strict';
    
    lore.initialize();
});